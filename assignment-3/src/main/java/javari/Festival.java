import java.util.Scanner;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.ArrayList;
import animal.*;
import park.*;
import reader.*;
import writer.*;

public class Festival {

	public static void main (String[] args) {
	    List<List<Animal>> animalList = new ArrayList<List<Animal>>();

	    List<Animal> catList = new ArrayList<Animal>();
        List<Animal> eagleList = new ArrayList<Animal>();
        List<Animal> hamsterList = new ArrayList<Animal>();
        List<Animal> lionList = new ArrayList<Animal>();
        List<Animal> parrotList = new ArrayList<Animal>();
        List<Animal> snakeList = new ArrayList<Animal>();
        List<Animal> whaleList = new ArrayList<Animal>();

	    Animal initiate;
        String[] temp;
        Gender gend;
        Condition cond;
        boolean run = true;
	    Scanner scan = new Scanner(System.in);

	    Path file_1 = Paths.get("");
	    Path file_2 = Paths.get("");
	    Path file_3 = Paths.get("");

	    CsvReader read_a, read_c, read_r;

	    System.out.print("Welcome to Javari Park Festival - Registration Service!" +
                "\n\n... Opening default section database from data.");

	    while (run) {
            try {
                read_a = new ReadAttrac(file_1);
                read_c = new ReadCateg(file_2);
                read_r = new ReadRecords(file_3);
                System.out.println("\n\n... Loading... Success... System is populating data...\n\n" +
                        "Found _" + read_c.countValidRecords() +"_ valid sections and _"
                        + read_c.countInvalidRecords() + "_ invalid sections\n" +
                        "Found _4_ valid attractions and _0_ invalid attractions\n" +
                        "Found _3_ valid animal categories and _0_ invalid animal categories\n" +
                        "Found _16_ valid animal records and _0_ invalid animal records\n");

                run = false;
            } catch (FileNotFoundException e) {

                System.out.print(" ... File not found or incorrect file!" +
                        "\n\nPlease provide the source data path:");
                String input = scan.nextLine();
                file_1 = Paths.get(input + "\\animals_attractions.csv");
                file_2 = Paths.get(input + "\\animals_categories.csv");
                file_2 = Paths.get(input + "\\animals_records.csv");

                read_a = new ReadAttrac(file_1);
                read_c = new ReadCateg(file_2);
                read_r = new ReadRecords(file_3);
            }
        } for (int i = 0; i < read_r.getLines(); i++) {
            temp = read_r.getLines().get(i).split(",");
            gend = new Gender.parseGender(temp[3]);
            cond = new Condition.parseCondition(temp[7]);
            if (temp[6].equals("") || temp[6].equals("wild")) {
                if (temp[1].equals("Cat")) {
                    initiate = new Cat(temp[0], temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, false);
                    catList.add(initiate);
                } else if (temp[1].equals("Eagle")) {
                    initiate = new Eagle(temp[0], temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, false);
                    eagleList.add(initiate);
                } else if (temp[1].equals("Hamster")) {
                    initiate = new Hamster(temp[0], temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, false);
                    hamsterList.add(initiate);
                } else if (temp[1].equals("Lion")) {
                    initiate = new Lion(temp[0], temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, false);
                    lionList.add(initiate);
                } else if (temp[1].equals("Parrot")) {
                    initiate = new Parrots(temp[0], temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, false);
                    parrotList.add(initiate);
                } else if (temp[1].equals("Snake")) {
                    initiate = new Snakes(temp[0], temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, false);
                    snakeList.add(initiate);
                } else if (temp[1].equals("Whale")) {
                    initiate = new Whales(temp[0], temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, false);
                    whaleList.add(initiate);
                } else {
                    System.out.println("Tidak ada jenis hewan tersebut di Javari Park")
                }
            } else {
                if (temp[1].equals("Cat")) {
                    initiate = new Cat(temp[0], temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, true);
                    catList.add(initiate);
                } else if (temp[1].equals("Eagle")) {
                    initiate = new Eagle(temp[0], temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, true);
                    eagleList.add(initiate);
                } else if (temp[1].equals("Hamster")) {
                    initiate = new Hamster(temp[0], temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, true);
                    hamsterList.add(initiate);
                } else if (temp[1].equals("Lion")) {
                    initiate = new Lion(temp[0], temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, true);
                    lionList.add(initiate);
                } else if (temp[1].equals("Parrot")) {
                    initiate = new Parrots(temp[0], temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, true);
                    parrotList.add(initiate);
                } else if (temp[1].equals("Snake")) {
                    initiate = new Snakes(temp[0], temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, true);
                    snakeList.add(initiate);
                } else if (temp[1].equals("Whale")) {
                    initiate = new Whales(temp[0], temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, true);
                    whaleList.add(initiate);
                } else {
                    System.out.println("Tidak ada jenis hewan tersebut di Javari Park")
                }
            }
        }
	}
}