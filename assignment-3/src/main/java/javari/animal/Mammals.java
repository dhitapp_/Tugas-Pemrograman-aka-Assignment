package javari.animal;

public abstract class Mammals extends Animal{
    public static final String[] type = {"Hamster", "Lion", "Cat", "Whale"};
    private boolean IsPregnant;

    public Mammals (Integer id, String type, String name, Gender gender, double length,
             double weight, Condition condition, boolean cond) {
        super(id, type, name, gender, length, weight, condition);
        this.IsPregnant = cond;
    }

    public boolean specificCondition() {
        return this.IsPregnant;
    }

    public abstract void attractionsOfMammal(int x);
}
