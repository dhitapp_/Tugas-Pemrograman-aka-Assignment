package javari.animal;

public class Whales extends Mammals{
    protected static final String[] attractions = {"Circle of Animal -> Whales", "Counting Masters -> Whales"};

    public Whales(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, boolean cond) {
        super(id, type, name, gender, length, weight, condition, cond);
    }

    public static String[] getAttractions() {
        return attractions;
    }
}
