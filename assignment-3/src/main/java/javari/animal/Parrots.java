package javari.animal;

public class Parrots extends Aves{
    protected static final String[] attractions = {"Dancing Animals", "Counting Masters"};

    public Parrots (Integer id, String type, String name, Gender gender, double length,
               double weight, Condition condition, boolean cond) {
        super(id, type, name, gender, length, weight, condition, cond);
    }

    public static String[] getAttractions() {
        return attractions;
    }
}
