package javari.animal;

public class Snakes extends Reptiles {
    protected static final String[] attractions = {"Dancing Animals -> Snakes", "Passionate Coders -> Snakes"};

    public Snakes (Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, boolean cond) {
        super(id, type, name, gender, length, weight, condition, cond);
    }

    public static String[] getAttractions() {
        return attractions;
    }
}
