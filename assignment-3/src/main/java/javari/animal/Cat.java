package javari.animal;

public class Cat extends Mammals{
    protected static final String[] attractions = {"Dancing Animals", "Passionate Coders"};

    public Cat(Integer id, String type, String name, Gender gender, double length,
               double weight, Condition condition, boolean cond) {
        super(id, type, name, gender, length, weight, condition, cond);
    }

    public static String[] getAttractions() {
        return attractions;
    }
}
