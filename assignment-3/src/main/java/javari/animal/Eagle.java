package javari.animal;

public class Eagle extends Aves {
    protected static final String[] attractions = {"Circle of Fire -> Eagle"};

    public Eagle (Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, boolean cond) {
        super(id, type, name, gender, length, weight, condition, cond);
    }

    public static String[] getAttractions() {
        return attractions;
    }
}
