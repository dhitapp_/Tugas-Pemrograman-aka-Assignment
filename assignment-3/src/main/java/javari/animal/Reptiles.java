package javari.animal;

public abstract class Reptiles extends Animal{
    public static final String[] type = {"Snake"};

    private boolean isTame;

    public  Reptiles (Integer id, String type, String name, Gender gender, double length,
                             double weight, Condition condition, boolean cond) {
        super(id, type, name, gender, length, weight, condition);
        this.isTame = cond;
    }

    public boolean specificCondition() {
        return this.isTame;
    }

    public abstract void attractionsOfReptile(int x);

}
