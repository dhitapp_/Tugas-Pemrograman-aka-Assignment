package javari.animal;

public class Lion extends Mammals {
    protected static final String[] attractions = {"Circle of Fire"};

    public Lion(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, boolean cond) {
        super(id, type, name, gender, length, weight, condition, cond);
    }

    public static String[] getAttractions() {
        return attractions;
    }
}
