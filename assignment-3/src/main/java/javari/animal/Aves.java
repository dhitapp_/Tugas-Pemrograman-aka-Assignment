package javari.animal;

public abstract class Aves extends Animal{
    public static final String[] type = {"Eagle", "Parrot"};
    private boolean layEggs;

    public Aves(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.layEggs = cond;
    }

    public boolean specificCondition() {
        return this.layEggs;
    }

    public abstract void attractionsOfAves(int x);
}
