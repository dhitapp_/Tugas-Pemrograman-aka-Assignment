package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class ReadRecords extends CsvReader {
    protected long valid, invalid;

    public ReadRecords (Path file) throws IOException {
        super(file);
        this.valid = 0;
        this.invalid = 0;
        this.validation();
    }

    public long countValidRecords() {
        return this.valid;
    }

    public long countInvalidRecords() {
        return this.invalid;
    }

    public boolean validation() {
        String[] temp = new String[8];
        for (int i = 0; i < this.getLines().size(); i++) {
            temp = this.getLines().get(i).split(CsvReader.COMMA);
            if (contentValid(temp)) {
                this.valid += 1;
            } else {
                this.invalid += 1;
            }
        } if (this.valid == this.getLines().size()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean contentValid(String[] checked) {
        if (((checked[6].equals("pregnant")) || (checked[6].equals(""))) && ((checked[1].equals("Whale"))
                || (checked[1].equals("Lion")) || (checked[1].equals("Cat")) || (checked[1].equals("Hamster")))) {
            return true;
        } else if (((checked[6].equals("tame")) || (checked[6].equals("wild"))) && ((checked[0].equals("Snake")))) {
            return true;
        } else if (((checked[6].equals("eggs")) || (checked[6].equals(""))) && ((checked[1].equals("Eagle"))
                || (checked[1].equals("Parrot")))) {
            return true;
        } return false;
    }
}