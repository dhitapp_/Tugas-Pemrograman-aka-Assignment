package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.ArrayList;

public class ReadCateg extends CsvReader {
    protected long valid, invalid;
    protected ArrayList<String> validList;

    public ReadCateg (Path file) throws IOException {
        super(file);
        this.valid = 0;
        this.invalid = 0;
        this.validList = new ArayList<String>();
        this.validation();
        this.Counting();
    }

    public long countValidRecords() {
        return this.valid;
    }

    public long countInvalidRecords() {
        return this.invalid;
    }

    public void Counting() {
        String[] memory = new String[3];
        for (int a = 0; a < this.getLines().size; a++) {
            memory = this.getLines().get(i).split(CsvReader.COMMA);
            if (this.validList.size() == 0) {
                validList.add(memory[1]);
            } else {
                if (!(validList.get(validList.size()-1).equals(memory[1]))) {
                    validList.add(memory[1]);
                } else {
                    this.invalid += 1;
                }
            }
        } this.valid = validList.size();
    }

    public void validation() {
        String[] temp = new String[3];
        int count = 0;
        for (int i = 0; i < this.getLines().size(); i++) {
            temp = this.getLines().get(i).split(CsvReader.COMMA);
            if (contentValid(temp)) {
                count += 1;
            }
        } if (count == this.getLines().size()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean contentValid(String[] checked) {
        if ((checked[1].equals("mammals")) && (checked[2].equals("Explore the Mammals"))
                && ((checked[0].equals("Lion")) || (checked[0].equals("Hamster"))
                || (checked[0].equals("Cat")) || (checked[0].equals("Whale")))) {
            return true;
        } else if ((checked[1].equals("aves")) && (checked[2].equals("World of Aves"))
                && ((checked[0].equals("Eagle")) || (checked[0].equals("Parrot")))) {
            return true;
        } else if ((checked[1].equals("reptiles")) && (checked[2].equals("Reptillian Kingdom"))
                && (checked[0].equals("Snake"))) {
            return true;
        }  return false;
    }
}