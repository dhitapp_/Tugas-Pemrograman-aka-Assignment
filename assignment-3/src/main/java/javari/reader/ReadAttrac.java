package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ReadAttrac extends CsvReader {
    protected long valid, invalid;
    protected ArrayList<String> validList;

	public ReadAttrac (Path file) throws IOException {
	    super(file);
        this.valid = 0;
        this.invalid = 0;
        this.validList = new ArayList<String>();
        this.validation();
        this.Counting();
    }

    public long countValidRecords() {
        return this.valid;
    }

    public void Counting() {
        String[] memory = new String[2];
        for (int a = 0; a < this.getLines().size; a++) {
            memory = this.getLines().get(i).split(CsvReader.COMMA);
            if (this.validList.size() == 0) {
                validList.add(memory[1]);
            } else {
                if (!(validList.get(validList.size()-1).equals(memory[1]))) {
                    validList.add(memory[1]);
                } else {
                    this.invalid += 1;
                }
            }
        } this.valid = validList.size();
    }

    public long countInvalidRecords() {
	    return this.invalid;
    }

    public void validation() {
	    String[] temp = new String[2];
	    int count = 0;
	    for (int i = 0; i < this.getLines().size(); i++) {
	        temp = this.getLines().get(i).split(CsvReader.COMMA);
	        if (contentValid(temp)) {
	            count += 1;
            }
        } if (count == this.getLines().size()) {
	        return true;
        } else {
	        return false;
        }
    }

    public boolean contentValid(String[] checked) {
	    if ((checked[1].equals("Circle of Fires")) && ((checked[0].equals("Whale"))
                || (checked[0].equals("Lion")) || (checked[0].equals("Eagle")))) {
	        return true;
        } else if ((checked[1].equals("Dancing Animals")) && ((checked[0].equals("Cat"))
                || (checked[0].equals("Snake")) || (checked[0].equals("Parrot")) || (checked[0].equals("Hamster")))) {
            return true;
        } else if ((checked[1].equals("Counting Masters")) && ((checked[0].equals("Hamster"))
                || (checked[0].equals("Whale")) || (checked[0].equals("Parrot")))) {
            return true;
        } else if ((checked[1].equals("Passionate Coders")) && ((checked[0].equals("Cat"))
                || (checked[0].equals("Hamster")) || (checked[0].equals("Snake")))) {
            return true;
        } return false;
    }
}