package javari.park;

import java.util.List;
import javari.animal.*;

public class Visited implements SelectedAttraction{
    private String type;
    private String attrac;
    private Animal performer;
    private List<Animal> getPerformer = new ArrayList<Animal>();

    public Visited (String type, String attractions) {
        this.type = type;
        this.attrac = attractions;
    }

    public Visited(Animal objek) {
        this.performer = objek;
    }

    public String getName() {
        return this.attrac;
    }

    public String getType() {
        return this.type;
    }

    public List<Animal> getPerformers() {
        return this.getPerformer;
    }

    public boolean addPerformer(Animal performer) {
        if (!(performer.specificAttractions()) && !(performer.getCondition().equals(Condition.HEALTHY_STR))) {
            this.getPerformer.add(performer);
            return true;
        } return false;
    }

}