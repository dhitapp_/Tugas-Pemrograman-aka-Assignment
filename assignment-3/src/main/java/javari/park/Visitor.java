package javari.park;

import java.util.List;

public class Visitor implements Registration{
    private String name;
    private int id;
    private List<SelectedAttraction> selectedAttractions = new ArrayList<SelectedAttraction>();

    public Visitor(int id, String name, List<SelectedAttraction> chosen) {
        this.id = id;
        this.name = name;
        this.selectedAttractions = chosen;
    }
    public int getRegistrationId() {
        return this.id;
    }

    public String getVisitorName() {
        return this.name;
    }

    public String setVisitorName(String nama) {
        this.name = nama;
    }

    public List<SelectedAttraction> getSelectedAttractions(){
        return this.selectedAttractions;
    }

    public boolean addSelectedAttraction(SelectedAttraction selected) {
        if (selected.getPeformer().size() != 0) {
            this.selectedAttractions.add(selected);
            return true;
        }
        return false;
    }
}