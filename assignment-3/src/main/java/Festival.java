import java.util.Scanner;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.ArrayList;
import javari.animal.*;
import javari.park.*;
import javari.reader.*;
import javari.writer.*;

public class Festival {

	public static void main (String[] args) {
	    List<Animal> catList = new ArrayList<Animal>();
        List<Animal> eagleList = new ArrayList<Animal>();
        List<Animal> hamsterList = new ArrayList<Animal>();
        List<Animal> lionList = new ArrayList<Animal>();
        List<Animal> parrotList = new ArrayList<Animal>();
        List<Animal> snakeList = new ArrayList<Animal>();
        List<Animal> whaleList = new ArrayList<Animal>();

        String command_1 = "Javari Park has 3 sections:\n" +
                "1. Explore the Mammals\n" +
                "2. World of Aves\n" +
                "3. Reptilian Kingdom\n" +
                "Please choose your preferred section (type the number): ";

	    Animal initiate;
        String[] temp;
        String command;
        SelectedAttraction binatang;

        int counter = 0;
        boolean run = true;
        boolean inp = true;

	    Scanner scan = new Scanner(System.in);

	    Path file_1 = Paths.get("");
	    Path file_2 = Paths.get("");
	    Path file_3 = Paths.get("");

	    CsvReader read_a, read_c, read_r;

	    System.out.print("Welcome to Javari Park Festival - Registration Service!" +
                "\n\n... Opening default section database from data.");

	    while (run) {
            try {
                read_a = new ReadAttrac(file_1);
                read_c = new ReadCateg(file_2);
                read_r = new ReadRecords(file_3);
                System.out.println("\n\n... Loading... Success... System is populating data...\n\n" +
                        "Found _" + read_c.countValidRecords() +"_ valid sections and _"
                        + read_c.countInvalidRecords() + "_ invalid sections\n" +
                        "Found _" + read_a.countValidRecords() + "_ valid attractions and _"
                        + read_a.countInvalidRecords() + "_ invalid attractions\n" +
                        "Found _" + read_c.countValidRecords() + "_ valid animal categories and _"
                        + read_c.countValidRecords() + "_ invalid animal categories\n" +
                        "Found _" + read_r.countValidRecords() +"_ valid animal records and _"
                        + read_r.countValidRecords() + "_ invalid animal records\n");
                run = false;
            } catch (FileNotFoundException e) {

                System.out.print(" ... File not found or incorrect file!" +
                        "\n\nPlease provide the source data path:");
                String input = scan.nextLine();
                file_1 = Paths.get(input + "\\animals_attractions.csv");
                file_2 = Paths.get(input + "\\animals_categories.csv");
                file_2 = Paths.get(input + "\\animals_records.csv");

                read_a = new ReadAttrac(file_1);
                read_c = new ReadCateg(file_2);
                read_r = new ReadRecords(file_3);
            }
        } for (int i = 0; i < read_r.getLines().size(); i++) {
            temp = read_r.getLines().get(i).split(",");
            Gender gend = Gender.parseGender(temp[3]);
            Condition cond = Condition.parseCondition(temp[7]);
            if (temp[6].equals("") || temp[6].equals("wild")) {
                if (temp[1].equals("Cat")) {
                    initiate = new Cat(Integer.parseInt(temp[0]), temp[1], temp[2], Gender.parseGender(temp[3]), Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, false);
                    catList.add(initiate);
                } else if (temp[1].equals("Eagle")) {
                    initiate = new Eagle(Integer.parseInt(temp[0]), temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, false);
                    eagleList.add(initiate);
                } else if (temp[1].equals("Hamster")) {
                    initiate = new Hamster(Integer.parseInt(temp[0]), temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, false);
                    hamsterList.add(initiate);
                } else if (temp[1].equals("Lion")) {
                    initiate = new Lion(Integer.parseInt(temp[0]), temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, false);
                    lionList.add(initiate);
                } else if (temp[1].equals("Parrot")) {
                    initiate = new Parrots(Integer.parseInt(temp[0]), temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, false);
                    parrotList.add(initiate);
                } else if (temp[1].equals("Snake")) {
                    initiate = new Snakes(Integer.parseInt(temp[0]), temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, false);
                    snakeList.add(initiate);
                } else if (temp[1].equals("Whale")) {
                    initiate = new Whales(Integer.parseInt(temp[0]), temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, false);
                    whaleList.add(initiate);
                } else {
                    System.out.println("Tidak ada jenis hewan tersebut di Javari Park");
                }
            } else {
                if (temp[1].equals("Cat")) {
                    initiate = new Cat(Integer.parseInt(temp[0]), temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, true);
                    catList.add(initiate);
                } else if (temp[1].equals("Eagle")) {
                    initiate = new Eagle(Integer.parseInt(temp[0]), temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, true);
                    eagleList.add(initiate);
                } else if (temp[1].equals("Hamster")) {
                    initiate = new Hamster(Integer.parseInt(temp[0]), temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, true);
                    hamsterList.add(initiate);
                } else if (temp[1].equals("Lion")) {
                    initiate = new Lion(Integer.parseInt(temp[0]), temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, true);
                    lionList.add(initiate);
                } else if (temp[1].equals("Parrot")) {
                    initiate = new Parrots(Integer.parseInt(temp[0]), temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, true);
                    parrotList.add(initiate);
                } else if (temp[1].equals("Snake")) {
                    initiate = new Snakes(Integer.parseInt(temp[0]), temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, true);
                    snakeList.add(initiate);
                } else if (temp[1].equals("Whale")) {
                    initiate = new Whales(Integer.parseInt(temp[0]), temp[1], temp[2], gend, Double.parseDouble(temp[4]),
                            Double.parseDouble(temp[5]), cond, true);
                    whaleList.add(initiate);
                } else {
                    System.out.println("Tidak ada jenis hewan tersebut di Javari Park");
                }
            }
        }

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n" +
                "\n" +
                "Please answer the questions by typing the number. " +
                "Type # if you want to return to the previous menu\n");
	    command = scan.nextLine();

	    label_1:
	    while (inp) {
            System.out.print(command_1);
            command = scan.nextLine();
            if (command.equals("*1*")) {
                System.out.print("\n\n--Explore the Mammals--" +
                        "\n1. Hamster\n" +
                        "2. Lion\n" +
                        "3. Cat\n" +
                        "4. Whale\n" +
                        "Please choose your preferred animals (type the number):");
                command = scan.nextLine();
                label_2:
                while (inp) {
                    if (command.equals("*1*")) {
                        for (int a = 0; a < hamsterList.size(); a++) {
                            binatang = new Visited(hamsterList.get(a));
                            if (binatang.addPerformer(hamsterList.get(a))) {
                                counter += 1;
                            } else {
                                System.out.println("\nUnfortunately, " +
                                        "no hamster can perform any attraction, please choose other animals");
                                break label_2;
                            }
                        }
                        for (int a = 0; a < Hamster.getAttractions().length; a++) {
                            System.out.println((a + 1) + ". " + Hamster.getAttractions()[a]);
                        }
                        System.out.print("Please choose your preferred attractions (type the number): ");
                        command = scan.nextLine();
                        if (command.equals("*1*")) {
                            binatang = new Visited("hamster", Hamster.getAttractions()[1]);
                        } else if (command.equals("*2*")) {
                            binatang = new Visited("hamster", Hamster.getAttractions()[2]);
                        } else if (command.equals("*3*")) {
                            binatang = new Visited("hamster", Hamster.getAttractions()[3]);
                        } else {
                            System.out.println("There is no such an attraction");
                        }
                    } else if (command.equals("*2*")) {
                        for (int a = 0; a < lionList.size(); a++) {
                            binatang = new Visited(lionList.get(a));
                            if (binatang.addPerformer()) {
                                counter += 1;
                            } else {
                                System.out.println("\nUnfortunately, " +
                                        "no lion can perform any attraction, please choose other animals");
                                break label_2;
                            }
                        }
                        for (int a = 0; a < Lion.getAttractions().length; a++) {
                            System.out.println((a + 1) + ". " + Lion.getAttractions()[a]);
                        }
                        System.out.print("Please choose your preferred attractions (type the number): ");
                        command = scan.nextLine();
                        if (command.equals("*1*")) {
                            binatang = new Visited("lion", Lion.getAttractions()[1]);
                        } else {
                            System.out.println("There is no such an attraction");
                        }
                    } else if (command.equals("*3*")) {
                        for (int a = 0; a < catList.size(); a++) {
                            binatang = new Visited(catList.get(a));
                            if (binatang.addPerformer()) {
                                counter += 1;
                            } else {
                                System.out.println("\nUnfortunately, " +
                                        "no cat can perform any attraction, please choose other animals");
                                break label_2;
                            }
                        }
                        for (int a = 0; a < Cat.getAttractions().length; a++) {
                            System.out.println((a + 1) + ". " + Cat.getAttractions()[a]);
                        }
                        System.out.print("Please choose your preferred attractions (type the number): ");
                        command = scan.nextLine();
                        if (command.equals("*1*")) {
                            binatang = new Visited("cat", Cat.getAttractions()[1]);
                        } else if (command.equals("*2*")) {
                            binatang = new Visited("cat", Cat.getAttractions()[2]);
                        } else {
                            System.out.println("There is no such an attraction");
                        }
                    } else if (command.equals("*4*")) {
                        for (int a = 0; a < whaleList.size(); a++) {
                            binatang = new Visited(whaleList.get(a));
                            if (binatang.addPerformer()) {
                                counter += 1;
                            } else {
                                System.out.println("\nUnfortunately, " +
                                        "no whale can perform any attraction, please choose other animals");
                                break label_2;
                            }
                        }
                        for (int a = 0; a < Whales.getAttractions().length; a++) {
                            System.out.println((a + 1) + ". " + Whales.getAttractions()[a]);
                        }
                        System.out.print("Please choose your preferred attractions (type the number): ");
                        command = scan.nextLine();
                        if (command.equals("*1*")) {
                            binatang = new Visited("whales", Whales.getAttractions()[1]);
                        } else if (command.equals("*2*")) {
                            binatang = new Visited("whales", Whales.getAttractions()[2]);
                        } else {
                            System.out.println("There is no such an attractions");
                        }
                    }
                }
            } else if (command.equals("*2*")) {
                System.out.print("\n\n--World of Aves--\n" +
                        "1. Eagle\n" +
                        "2. Parrot\n" +
                        "Please choose your preferred animals (type the number):");
            } else if (command.equals("*3*")) {
                System.out.print("\n\n--Reptillian Kingdom--" +
                        "\n1. Snake\n" +
                        "Please choose your preferred animals (type the number):");

            }
        }
    }
}