// build class to manage the game
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class MatchPairBoard extends JFrame{
    private int counter = 0;
    private JLabel label = new JLabel("Number of tries: 0");
    private JPanel windowPanel;
    private List<String> fileName = new ArrayList<String>();
    private ImageIcon front;
    private IconButton selected, firstCard, secCard;
    private Timer tkeeper;
    private Container container;
    private List<IconButton> theButton = new ArrayList<IconButton>();

    // Constructor
    public MatchPairBoard() {

        this.counter = 0;

        // set the panell

        JPanel playOrEx = new JPanel();
        playOrEx.setLayout(new FlowLayout(FlowLayout.CENTER));

        windowPanel = new JPanel();
        windowPanel.setLayout(new GridLayout(6,6));

        container = getContentPane();

        // Store the file name
        for (int i = 1; i <= 18; i++) {
            fileName.add("D:\\ILKOM SMT 2\\DDP 2\\Tugas Pemrograman\\Tugas-Pemrograman-aka-Assignment\\assignment-4\\src\\kitty (" + i + ").png");
        }

        // set the image to be the frontground
        Image tempor = (new ImageIcon("D:\\ILKOM SMT 2\\DDP 2\\Tugas Pemrograman\\Tugas-Pemrograman-aka-Assignment\\assignment-4\\src\\cat-walk.png")).getImage();
        front = new ImageIcon(tempor.getScaledInstance(80,80, Image.SCALE_DEFAULT));

        // iteration to make the button 36 times
        for (int j = 0; j < fileName.size(); j++) {
            for (int k = 0; k < 2; k++) {
                IconButton tempo = new IconButton(fileName.get(j));
                theButton.add(tempo);
                tempo.setIcon(front);
                tempo.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        selected = tempo;
                        selected.setIcon(selected.getCard());
                        turnTheCard();
                    }
                });
            }
        }

        //shuffling the Buttons
        Collections.shuffle(theButton);

        // for each button, add it to the panel
        for (IconButton c: theButton) {
            windowPanel.add(c);
        }

        // set Timer for cards to be checked then closed
        tkeeper = new Timer(700, new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                checkCards();
            }
        });
        tkeeper.setRepeats(false);

        // make playAgain's button and exit's button and add label to the panel
        JButton again = new JButton("Play Again?");
        again.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                restart();
            }
        });
        playOrEx.add(again, BorderLayout.CENTER);

        JButton exit = new JButton("Exit");
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        playOrEx.add(exit);
        playOrEx.add(label, BorderLayout.PAGE_END);

        //add the panels to the container
        container.add(windowPanel);
        container.add(playOrEx, BorderLayout.PAGE_END);

    }

    //method to restart the game
    public void restart() {
        this.counter = 0;
        Collections.shuffle(fileName);
        for (int a = 0; a < fileName.size(); a++) {
            for (int b = 0; b < 2; b++) {
                theButton.get(b+a).setIcon(front);
                theButton.get(b+a).setCard(fileName.get(a));
                theButton.get(b+a).setMatched(false);
                theButton.get(b+a).setVisible(true);
            }
            label.setText("Number of tries: " + this.counter);
        } Collections.shuffle(theButton);

    }

    // method to turn the card after being clicked
    public void turnTheCard() {

        if (firstCard == null && secCard == null) {
            firstCard = selected;
        }

        if (firstCard != null && firstCard != selected && secCard == null){
            secCard = selected;
            this.counter += 1;
            tkeeper.start();
        }
        label.setText("Number of tries: " + this.counter);
    }

    // method to check the cards are being matched or not
    public void checkCards() {
        if (firstCard.getFileName().equals(secCard.getFileName())) {
            firstCard.setMatched(true);
            secCard.setMatched(true);
            secCard.setVisible(false);
            firstCard.setVisible(false);
            if (isWon()) {
                JOptionPane.showMessageDialog(null, "You win!");
                System.exit(0);
            }
            firstCard = null;
            secCard = null;

        } else {
            firstCard.setIcon(front);
            secCard.setIcon(front);
            firstCard = null;
            secCard = null;
        }
    }

    // method to check is the player already won the game or not
    public boolean isWon() {
        for (IconButton i: theButton) {
            if (!i.getMatched() ) {
                return false;
            }
        } return true;
    }

}