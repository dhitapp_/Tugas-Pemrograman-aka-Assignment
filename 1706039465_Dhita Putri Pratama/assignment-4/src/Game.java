import javax.swing.*;
import java.awt.*;

public class Game {

    //Method for to run the code

    public static void main (String[] args) {
        MatchPairBoard board = new MatchPairBoard();
        board.setPreferredSize(new Dimension(600,600));
        board.setLocation(500, 250);
        board.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        board.pack();
        board.setVisible(true);
        board.setTitle("Find all the kitties!");
        board.setLayout(new BorderLayout());
    }

}