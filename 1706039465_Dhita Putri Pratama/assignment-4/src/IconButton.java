// Class to manage the buttons or the card to be played

import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.*;

public class IconButton extends JButton {
    private ImageIcon card;
    private String file;
    private final JButton button = new JButton();
    private boolean matched;

    // Constructor
    public IconButton (String theFile) {
        this.file = theFile;
        setCard(theFile);
    }

    // Getters and setters
    public ImageIcon getCard() {
        return this.card;
    }

    public String getFileName() {
        return this.file;
    }
    public void setFileName(String c) {
        this.file = c;
    }

    public void setMatched(boolean matched){

        this.matched = matched;
    }

    public void setCard(String c) {
        this.setFileName(c);
        Image theImage = (new ImageIcon(c).getImage()).getScaledInstance(80,80, Image.SCALE_DEFAULT);
        this.card = new ImageIcon(theImage);
    }

    public boolean getMatched(){
        return this.matched;
    }
}
