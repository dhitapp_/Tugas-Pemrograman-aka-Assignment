import java.util.ArrayList;
import java.util.List;

public class Eagle extends Animal {
    public static List<Animal> tempo = new ArrayList<Animal>();

    public Eagle (String name, int length, boolean IndoorOr) {
        super(name, length, IndoorOr);
        tempo.add(new Animal(name, length, IndoorOr));
    }

    public String flyHigh() {
        return this.getName() + " makes a voice: kwaakk...\nYou hurt!";
    }
}
