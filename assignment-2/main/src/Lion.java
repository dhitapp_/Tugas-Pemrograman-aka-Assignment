import java.util.ArrayList;
import java.util.List;

public class Lion extends Animal{
    public static List<Animal> tempo = new ArrayList<Animal>();

    public Lion (String name, int length, boolean IndoorOr) {
        super(name, length, IndoorOr);
        tempo.add(new Animal(name, length, IndoorOr));
    }

    public String Hunting() {
        return "Lion is hunting..\n"
                + this.getName() + " makes a voice: err...!";
    }

    public String Brush() {
        return "Clean the lion's mane..\n"
                + this.getName() + " makes a voice: Hauhhmm!";
    }

    public String Disturb() {
        return this.getName() + " makes a voice: HAUHHMM!";
    }
}
