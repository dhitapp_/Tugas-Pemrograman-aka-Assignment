import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class Cage {

    protected static List<List<String>> level1 = new ArrayList<List<String>>();
    protected static List<List<String>> level2 = new ArrayList<List<String>>();
    protected static List<List<String>> level3 = new ArrayList<List<String>>();

    public static void sortCage() {
        List<String> temporary_1 = new ArrayList<String>();
        List<String> temporary_2 = new ArrayList<String>();
        List<String> temporary_3 = new ArrayList<String>();
        List<String> temp2 = new ArrayList<>();
        List<String> temp3 = new ArrayList<>();
        Animal.setStores();

        int elements = 0;
        for ( int i = 0; i < Animal.getStores().size(); i++) {
            elements = Animal.getStores().get(i).size();
            if (elements > 0) {
                temporary_1 = new ArrayList<String>();
                temporary_2 = new ArrayList<String>();
                temporary_3 = new ArrayList<String>();

                System.out.println("location :" + Animal.getStores().get(i).get(0).getLoc() + "\n");
                for (int j = 0; j < elements; j++) {
                    if (elements%3 == 0) {
                        if (j / (elements/3) == 0) {
                            temporary_1.add(Animal.getStores().get(i).get(j).getInfo());
                        } else if (j / (elements/3) == 1) {
                            temporary_2.add(Animal.getStores().get(i).get(j).getInfo());
                        } else {
                            temporary_3.add(Animal.getStores().get(i).get(j).getInfo());
                        }
                    } else if (elements%3 == 1) {
                        if (elements/3 == 0) {
                            temporary_1.add(Animal.getStores().get(i).get(j).getInfo());
                        } else {
                            if (j%3 == 0 && j != 0 && j == elements-1) {
                                temporary_3.add(Animal.getStores().get(i).get(j).getInfo());
                            } else {
                                if (j / (elements/3) == 0) {
                                    temporary_1.add(Animal.getStores().get(i).get(j).getInfo());
                                } else if (j / (elements/3) == 1) {
                                    temporary_2.add(Animal.getStores().get(i).get(j).getInfo());
                                } else {
                                    temporary_3.add(Animal.getStores().get(i).get(j).getInfo());
                                }
                            }
                        }
                    } else if (elements % 3 == 2) {
                        if (elements/3 == 0) {
                            if ( j == 0) {
                                temporary_1.add(Animal.getStores().get(i).get(j).getInfo());
                            } else {
                                temporary_2.add(Animal.getStores().get(i).get(j).getInfo());
                            }
                        } else {
                            if (j%3 == 0 && j != 0 && j == elements-1) {
                                temporary_3.add(Animal.getStores().get(i).get(j).getInfo());
                            } else if (j%3 == 1 && j != 1 && j == elements-2) {
                                temporary_2.add(Animal.getStores().get(i).get(j).getInfo());
                            } else {
                                if (j / (elements/3) == 0) {
                                    temporary_1.add(Animal.getStores().get(i).get(j).getInfo());
                                } else if (j / (elements/3) == 1) {
                                    temporary_2.add(Animal.getStores().get(i).get(j).getInfo());
                                } else {
                                    temporary_3.add(Animal.getStores().get(i).get(j).getInfo());
                                }
                            }
                        }
                    }
                }
                System.out.print("level 3: ");
                for (int s = 0; s < temporary_3.size(); s ++) {
                    System.out.print(temporary_3.get(s) + ", ");
                }
                System.out.print("\nlevel 2: ");
                for (int s = 0; s < temporary_2.size(); s ++) {
                    System.out.print(temporary_2.get(s) + ", ");
                }
                System.out.print("\nlevel 1: ");
                for (int s = 0; s < temporary_1.size(); s ++) {
                    System.out.print(temporary_1.get(s) + ", ");
                }

                //reverse
                Collections.reverse(temporary_3);
                Collections.reverse(temporary_2);
                Collections.reverse(temporary_1);

                temp2 = temporary_2;
                temp3 = temporary_3;

                temporary_2 = temporary_1;
                temporary_3 = temp2;
                temporary_1 = temp3;

                System.out.println("\n\nAfter rearrangement...");
                System.out.print("level 3: ");
                for (int s = 0; s < temporary_3.size(); s ++) {
                    System.out.print(temporary_3.get(s) + ", ");
                }
                System.out.print("\nlevel 2: ");
                for (int s = 0; s < temporary_2.size(); s ++) {
                    System.out.print(temporary_2.get(s) + ", ");
                }
                System.out.print("\nlevel 1: ");
                for (int s = 0; s < temporary_1.size(); s ++) {
                    System.out.print(temporary_1.get(s) + ", ");
                } System.out.println("\n");
            }
        }
    }


}
