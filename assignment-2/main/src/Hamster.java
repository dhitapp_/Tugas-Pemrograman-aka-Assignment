import java.util.ArrayList;
import java.util.List;

public class Hamster extends Animal{
    public static List<Animal> tempo = new ArrayList<Animal>();

    public Hamster (String name, int length, boolean IndoorOr) {
        super(name, length, IndoorOr);
        tempo.add(new Animal(name, length, IndoorOr));
    }

    public String Gnaw() {
        return this.getName() + " makes a voice: ngkkrit.. ngkkrrriiit";
    }

    public String Run() {
        return this.getName() + " makes a voice: trrr... trrr...";
    }
}
