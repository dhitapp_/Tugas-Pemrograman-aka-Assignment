import java.util.ArrayList;
import java.util.List;

public class Animal {
    private boolean isIndoor;
    private char cage;
    private int length;
    private String name;
    private String info;
    private String loc;
    private Animal cats ;
    private Animal lion;
    private Animal eagle;
    private Animal parrot;
    private Animal hamster;
    protected static List<List<Animal>> stores = new ArrayList<List<Animal>>();

    public Animal (String name, int length, boolean IndoorOr) {
        this.name = name;
        this.length = length;
        this.isIndoor = IndoorOr;
        this.doThis(IndoorOr);
    }

    public Animal (Cat objek) {
        this.cats = objek;
    }

    public Animal (Lion objek) {
        this.lion = objek;
    }

    public Animal (Eagle objek) {
        this.eagle = objek;
    }

    public Animal (Parrot objek) {
        this.parrot = objek;
    }

    public Animal (Hamster objek) {
        this.hamster = hamster;
    }

    public void doThis(boolean cond) {
        this.setLoc(cond);
        this.setCage();
        this.setInfo();
    }

    public String getName() {
        return this.name;
    }

    public int getLength() {
        return this.length;
    }

    public String getLoc() {
        return this.loc;
    }

    public boolean getIsIndoor() {
        return this.isIndoor;
    }

    public char getCage() {
        return this.cage;
    }
    public String getInfo() {
        return this.info;
    }

    public static List<List<Animal>> getStores() {
        return Animal.stores;
    }

    public void setLoc(boolean exam) {
        if (exam) {
            this.loc = "indoor";
        } else {
            this.loc = "outdoor";
        }
    }

    public void setCage() {
        if (this.getIsIndoor()) {
            if (this.getLength() < 45) {
                this.cage = 'A';
            } else if (this.getLength() >= 45 && this.getLength() <= 60) {
                this.cage = 'B';
            } else {
                this.cage = 'C';
            }
        } else {
            if (this.getLength() < 75) {
                this.cage = 'A';
            } else if (this.getLength() >= 75 && this.getLength() <= 90) {
                this.cage = 'B';
            } else {
                this.cage = 'C';
            }
        }
    }

    public void setInfo() {
        this.info = this.getName() + " (" + this.getLength() + " - " + this.getCage() + ")";
    }

    public static void setStores() {
        Animal.stores.add(Cat.tempo);
        Animal.stores.add(Lion.tempo);
        Animal.stores.add(Eagle.tempo);
        Animal.stores.add(Parrot.tempo);
        Animal.stores.add(Hamster.tempo);
    }


}
