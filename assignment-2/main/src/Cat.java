import java.util.Random;
import java.util.List;
import java.util.ArrayList;

public class Cat extends Animal {
    private static final String[] response = {"Miaaaw...", "Purrr...", "Mwaw!", "Mraaawr!"};
    public static List<Animal>tempo = new ArrayList<Animal>();

    public Cat (String name, int length, boolean IndoorOr) {
        super(name, length, IndoorOr);
        tempo.add(new Animal(name, length, IndoorOr));

    }

    public String brushTheFur() {
        return "Time to brush " + this.getName() + "'s fur\n"
                + this.getName() + " makes a voice: Nyaaan...";
    }

    public String Cuddle() {
        Random random = new Random();
        int value = random.nextInt(4);
        return this.getName() + " makes a voice: " + Cat.response[value];
    }
}
