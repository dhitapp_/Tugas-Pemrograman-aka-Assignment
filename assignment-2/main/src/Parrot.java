import java.util.ArrayList;
import java.util.List;

public class Parrot extends Animal{
    public static List<Animal> tempo = new ArrayList<Animal>();

    public Parrot (String name, int length, boolean IndoorOr) {
        super(name, length, IndoorOr);
        tempo.add(new Animal(name, length, IndoorOr));
    }

    public String flyLow() {
        return "Parrot " + this.getName() + " flies!\n"
                + this.getName() + " makes a voice: FLYYYY....";
    }

    public String Resemble(String words) {
        return this.getName() + " says: " + words.toUpperCase();
    }
}
