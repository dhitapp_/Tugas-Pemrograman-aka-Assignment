import java.util.Scanner;
import java.util.ArrayList;

public class JavariPark {

    public static void main(String[] args) {
        ArrayList<Animal> cats_init = new ArrayList<Animal>();
        ArrayList<Animal> lions_init = new ArrayList<Animal>();
        ArrayList<Animal> parrots_init = new ArrayList<Animal>();
        ArrayList<Animal> hamsters_init = new ArrayList<Animal>();
        ArrayList<Animal> eagles_init = new ArrayList<Animal>();
        ArrayList<Cat> cats = new ArrayList<Cat>();
        ArrayList<Lion> lions = new ArrayList<Lion>();
        ArrayList<Parrot> parrots = new ArrayList<Parrot>();
        ArrayList<Hamster> hamsters = new ArrayList<Hamster>();
        ArrayList<Eagle> eagles = new ArrayList<Eagle>();

        boolean run = true;
        Scanner input = new Scanner (System.in);
        System.out.println("Welcome to Javari Park!" + "\nInput the number of animals");

        System.out.print("cat: ");
        int much = Integer.parseInt(input.nextLine());

        if (much > 0) {
            System.out.println("Provide the information of cat(s):");
            String init = input.nextLine();
            String[] splt = init.split(",");
            String[][] nice = new String[splt.length][2];
            for (int a = 0; a < splt.length ; a++) {
                nice[a] = splt[a].split("[|]");
            }
            for (int b = 0; b < much; b++) {
                cats.add(new Cat(nice[b][0], Integer.parseInt(nice[b][1]), true));
                cats_init.add(cats.get(b));
            }
        }

        System.out.print("lion: ");
        much = Integer.parseInt(input.nextLine());

        if (much > 0) {
            System.out.println("Provide the information of lion(s):");
            String init = input.nextLine();
            String[] splt = init.split(",");
            String[][] nice = new String[splt.length][2];
            for (int a = 0; a < splt.length ; a++) {
                nice[a] = splt[a].split("[|]");
            }
            for (int b = 0; b < much; b++) {
                lions.add(new Lion(nice[b][0], Integer.parseInt(nice[b][1]), false));
                lions_init.add(lions.get(b));
            }
        }

        System.out.print("eagle: ");
        much = Integer.parseInt(input.nextLine());

        if (much > 0) {
            System.out.println("Provide the information of eagle(s):");
            String init = input.nextLine();
            String[] splt = init.split(",");
            String[][] nice = new String[splt.length][2];
            for (int a = 0; a < splt.length ; a++) {
                nice[a] = splt[a].split("[|]");
            }
            for (int b = 0; b < much; b++) {
                eagles.add(new Eagle(nice[b][0], Integer.parseInt(nice[b][1]), false));
                eagles_init.add(eagles.get(b));
            }
        }

        System.out.print("parrot: ");
        much = Integer.parseInt(input.nextLine());

        if (much > 0) {
            System.out.println("Provide the information of parrot(s):");
            String init = input.nextLine();
            String[] splt = init.split(",");
            String[][] nice = new String[splt.length][2];
            for (int a = 0; a < splt.length ; a++) {
                nice[a] = splt[a].split("[|]");
            }
            for (int b = 0; b < much; b++) {
                parrots.add(new Parrot(nice[b][0], Integer.parseInt(nice[b][1]), true));
                parrots_init.add(parrots.get(b));
            }
        }

        System.out.print("hamster: ");
        much = Integer.parseInt(input.nextLine());

        if (much > 0) {
            System.out.println("Provide the information of hamster(s):");
            String init = input.nextLine();
            String[] splt = init.split(",");
            String[][] nice = new String[splt.length][2];
            for (int a = 0; a < splt.length ; a++) {
                nice[a] = splt[a].split("[|]");
            }
            for (int b = 0; b < much; b++) {
                hamsters.add(new Hamster(nice[b][0], Integer.parseInt(nice[b][1]), true));
                hamsters_init.add(hamsters.get(b));
            }
        }

        System.out.println("Animal has been successfully recorded!"
                + "\n=============================================");

        //Cage

        System.out.println("Cage arangement");

        Cage.sortCage();

        System.out.println("\nANIMALS NUMBER:\ncat:" + cats.size()
                + "\nlion:" + lions.size()
                + "\nparrot:" + parrots.size()
                + "\neagle:" + eagles.size()
                + "\nhamster:" + hamsters.size()
                + "\n\n=============================================");

        int masuk = 0;
        int command = 0;
        String nama = "";
        String words = "";
        while (run) {
            System.out.println("Which animal you want to visit?"
                    + "\n(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)");
            masuk = Integer.parseInt(input.nextLine());

            if (masuk == 1) {
                System.out.print("Mention the name of cat you want to visit: ");
                nama = input.nextLine();

                labelOne:
                for (int c = 0; c < cats.size() ; c++) {
                    if (c == cats.size()-1 && !(nama.equals(cats.get(c).getName()))) {
                        System.out.println("There is no cat with that name!"
                                + "\nBack to the office!\n");
                    } else if (nama.equals(cats.get(c).getName())) {
                        System.out.println("You are visiting " + nama + " (cat) now"
                                + ", what would you like to do?"
                                + "\n1: Brush the fur 2: Cuddle");
                        command = Integer.parseInt(input.nextLine());
                        if (command == 1) {
                            System.out.println(cats.get(c).brushTheFur());
                        } else if (command == 2) {
                            System.out.println(cats.get(c).Cuddle());
                        } else {
                            System.out.println("You do nothing...");
                        } System.out.println("Back to the office!\n");
                        break labelOne;
                    }
                }
            } else if (masuk == 2) {
                System.out.print("Mention the name of eagle you want to visit: ");
                nama = input.nextLine();

                labelOne:
                for (int c = 0; c < eagles.size() ; c++) {
                    if (c == eagles.size()-1 && !(nama.equals(eagles.get(c).getName()))) {
                        System.out.println("There is no eagle with that name!"
                                + "\nBack to the office!\n");
                    } else if (nama.equals(eagles.get(c).getName())) {
                        System.out.println("You are visiting " + nama + " (eagle) now"
                                + ", what would you like to do?"
                                + "\n1: Order to fly");
                        command = Integer.parseInt(input.nextLine());
                        if (command == 1) {
                            System.out.println(eagles.get(c).flyHigh());
                        } else {
                            System.out.println("You do nothing...");
                        } System.out.println("Back to the office!\n");
                        break labelOne;
                    } else {
                        System.out.println("test");
                    }
                }
            } else if (masuk == 3) {
                System.out.print("Mention the name of hamster you want to visit: ");
                nama = input.nextLine();

                labelOne:
                for (int c = 0; c < hamsters.size() ; c++) {
                    if (c == hamsters.size()-1 && !(nama.equals(hamsters.get(c).getName()))) {
                        System.out.println("There is no hamster with that name!"
                                + "\nBack to the office!\n");
                    } else if (nama.equals(hamsters.get(c).getName())) {
                        System.out.println("You are visiting " + nama + " (hamster) now"
                                + ", what would you like to do?"
                                + "\n1: See it gnawing 2: Order to run in the hamster wheel");
                        command = Integer.parseInt(input.nextLine());
                        if (command == 1) {
                            System.out.println(hamsters.get(c).Gnaw());
                        } else if (command == 2) {
                            System.out.println(hamsters.get(c).Run());
                        } else {
                            System.out.println("You do nothing...");
                        } System.out.println("Back to the office!\n");
                        break labelOne;
                    }
                }
            } else if (masuk ==4) {
                System.out.print("Mention the name of parrot you want to visit: ");
                nama = input.nextLine();

                labelOne:
                for (int c = 0; c < parrots.size() ; c++) {
                    if (c == parrots.size()-1 && !(nama.equals(parrots.get(c).getName()))) {
                        System.out.println("There is no parrot with that name!"
                                + "\nBack to the office!\n");
                    } else if (nama.equals(parrots.get(c).getName())) {
                        System.out.println("You are visiting " + nama + " (parrot) now"
                                + ", what would you like to do?"
                                + "\n1: Order to fly 2: Do conversation");
                        command = Integer.parseInt(input.nextLine());
                        if (command == 1) {
                            System.out.println(parrots.get(c).flyLow());
                        } else if (command == 2) {
                            System.out.print("You say: ");
                            words = input.nextLine();
                            System.out.println(parrots.get(c).Resemble(words));
                        } else {
                            System.out.println(nama + " says: HM?");
                        } System.out.println("Back to the office!\n");
                        break labelOne;
                    }
                }
            } else if (masuk == 5) {
                System.out.print("Mention the name of lion you want to visit: ");
                nama = input.nextLine();

                labelOne:
                for (int c = 0; c < lions.size() ; c++) {
                    if (c == lions.size()-1 && !(nama.equals(lions.get(c).getName()))) {
                        System.out.println("There is no lion with that name!"
                                + "\nBack to the office!\n");
                    } else if (nama.equals(lions.get(c).getName())) {
                        System.out.println("You are visiting " + nama + " (lion) now"
                                + ", what would you like to do?"
                                + "\n1: See it hunting 2: Brush the mane 3: Disturb it");
                        command = Integer.parseInt(input.nextLine());
                        if (command == 1) {
                            System.out.println(lions.get(c).Hunting());
                        } else if (command == 2) {
                            System.out.println(lions.get(c).Brush());
                        } else if (command == 3) {
                            System.out.println(lions.get(c).Disturb());
                        } else {
                            System.out.println("You do nothing...");
                        } System.out.println("Back to the office!\n");
                        break labelOne;
                    }
                }
            } else if (masuk == 99) {
                System.out.println("Good bye!");
                run = false;
                break;
            } else {
                System.out.println("Please input the right command");
            }
        }
    }
}
