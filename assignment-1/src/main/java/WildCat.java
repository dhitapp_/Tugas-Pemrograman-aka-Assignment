/*Nama  : Dhita Putri Pratama
 *NPM    : 1706039465
 *Kelas  : C DDP-2
 *Asdos  : Kak Zaky*/


//membuat kelas WildCat sebagai data-data dari kucing
public class WildCat {
    //class variable
    public String name;
    public double weight; // In kilograms
    public double length; // In centimeters

    //constructor overloading
    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    // menghitung BMI dari suatu objek kucing dengan tipe data double
    public double computeMassIndex() {
        double bmi = (double) (this.weight / (double) (this.length * this.length * 0.0001));
        return bmi;
    }
}
