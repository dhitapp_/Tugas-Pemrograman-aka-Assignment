/*Nama  : Dhita Putri Pratama
 *NPM    : 1706039465
 *Kelas  : C DDP-2
 *Asdos  : Kak Zaky*/


//import kelas Scanner

import java.util.Scanner;

//membuat kelas A1Station yang digunakan sebagai main method
public class A1Station {

    //class variables
    private static final double THRESHOLD = 250; // in kilograms
    private static TrainCar[] car;
    private static WildCat cat;
    private static TrainCar next;
    private static boolean full = true;

    // main method untuk menjalankan program
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int number = Integer.parseInt(input.nextLine());
        car = new TrainCar[number];
        double count = 0;
        for (int a = 0; a < number; a++) {
            String data = input.nextLine();
            String[] splited = data.split(",");
            //inisiasi objek WildCat
            double weight = Double.parseDouble(splited[1]);
            double length = Double.parseDouble(splited[2]);
            cat = new WildCat(splited[0], weight, length);
            //inisiasi objek TrainCar
            if (getNext() == null && full) {
                car[a] = new TrainCar(cat);
                setFull(false);
                count = 1;
            } else {
                car[a] = new TrainCar(cat, getNext());
                count += 1;
            }
            setNext(car[a]);
            getNext().setAverageBmi(count);
            if (getNext().computeTotalWeight() > THRESHOLD || a == number - 1) {
                display();
                setNext(null);
                setFull(true);
                count = 0;
            }
        }
        input.close();
    }

    //getters and setters
    public static void setFull(boolean full) {
        full = full;
    }

    public static TrainCar getNext() {
        return next;
    }

    public static void setNext(TrainCar object) {
        next = object;
    }

    //method untuk mencetak bagian awal dari printCar()
    public static void display() {
        System.out.print("The train departs to Javari Park\n[LOCO]<--");
        getNext().printCar();
        System.out.printf("Average mass index of all cats: %.2f\n", getNext().getAverageBmi());
        System.out.print("In average, the cats in the train are *");
        System.out.println(getNext().category(getNext().getAverageBmi()));
    }
}