/*Nama  : Dhita Putri Pratama
 *NPM    : 1706039465
 *Kelas  : C DDP-2
 *Asdos  : Kak Zaky*/

//membuat kelas TrainCar untuk mengangkut kucing di mobil tersebut
public class TrainCar {

    //variable yang akan digunakan
    public static final double EMPTY_WEIGHT = 20; // In kilograms
    static double average = 0;
    static double count = 0;
    public WildCat cat;
    public TrainCar next;

    //constructors dengan 1 parameter. Digunakan saat pertama kali inisiasi
    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    //constructor dengan 2 parameter. Digunakan ketika sudah melakukan inisiasi sebelumnya
    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    // Getters and Setters
    public double getAverageBmi() {
        return average;
    }

    public void setAverageBmi(double count) {
        average = (this.computeTotalMassIndex() / count);
    }

    //menghitung berat setiap kucing dan mobilnya dengan rekursif
    public double computeTotalWeight() {
        if (this.next == null) {
            return TrainCar.EMPTY_WEIGHT + this.cat.weight;
        } else {
            return TrainCar.EMPTY_WEIGHT + this.cat.weight + this.next.computeTotalWeight();
        }
    }

    //menghitung BMI setiap kucing dengan rekursif
    public double computeTotalMassIndex() {
        if (this.next == null) {
            return this.cat.computeMassIndex();
        } else {
            return this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
        }
    }

    //mengembalikan nilai kategori berupa String dari rata-rata BMI kucing yang terinisiasi
    public String category(double average) {
        String catec = "";
        if (average < 18.5) {
            catec = "underweight";
        } else if (average >= 18.5 && average < 25) {
            catec = "normal";
        } else if (average >= 25 && average < 30) {
            catec = "overweight";
        } else if (average >= 30) {
            catec = "obese";
        }
        return catec;
    }

    //mencetak mobil-mobil dan kucingnya
    public void printCar() {
        if (this.next == null) {
            System.out.println("(" + this.cat.name + ")");
        } else {
            System.out.print("(" + this.cat.name + ")--");
            this.next.printCar();
        }
    }
}
